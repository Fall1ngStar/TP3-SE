#ifndef SELL_H
#define SELL_H

void analyseCommand(char **command);

void executeCommand(char **command);

// Create the redirections of the given command from left to right
void createRedirections(char **command);

// If there is no file after the redirection, send an error and exit the current processus
void sendParseError();

// Check for the specified chars in the current string
// The returned int depends of the redirection chars, and is 0 when there is none
int checkSpecialChar(char *str);

// Log message if in debug mode
void debug(char* str);

// Create a redirection of strio input stream to path file, with flag and createMode when necessary
void createRedir(char *path, int flag, int createMode, int stdio);

//Split the given command on the first redirection chars
//Puts a NULL pointer at the redirection chars location and return the next pointer
char **splitOnRedir(char **command);
#endif