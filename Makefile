HEADERS = $(shell echo *.h)
OBJECT = $(shell echo *.c)
OBJECTS = $(addsuffix .o, $(OBJECT))

default: shell

%.c.o: %.c $(HEADERS)
	gcc -c $< -o $@

shell: $(OBJECTS)
	gcc $(OBJECTS) -o $@

clean:
	-rm -f $(OBJECTS)
	-rm -f shell
	-rm -f *~
