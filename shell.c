#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>

#include "ligne_commande.h"
#include "shell.h"

#define DEBUG 1

void analyseCommand(char **command)
{
    if (strcmp(command[0], "exit") == 0)
    {
        // Quitte le shell
        exit(0);
    }
    else if (strcmp(command[0], "cd") == 0)
    {
        // Change le répertoire courant
        if (chdir(command[1]) < 0)
        {
            perror("cd");
        }
    }
    else if (strcmp(command[0], "export") == 0)
    {
        // Ajoute une variable à l'environnement
        char *value = separe_egal(command[1]);
        if (setenv(command[1], value, 1) < 0)
        {
            perror("export:");
        }
    }
    // Pas une commande interne donc execution normale
    else
    {
        executeCommand(command);
    }
}
void executeCommand(char **command)
{
    pid_t pid;
    int status;
    pid = fork();
    // Processus fils
    if (pid == 0)
    {
        // Crée les redirections
        createRedirections(command);
        // Enlève les redirections pour qu'elles ne soient pas passées en argument de la commande
        char **remaining = splitOnRedir(command);
        int retour = execvp(command[0], command);
        if (retour == -1)
            perror("\033[0;31mCommand\033[0m");
        exit(-1);
    }
    // Processus père
    else
    {
        waitpid(pid, &status, 0);
    }
}

// Découpe la chaine donnée  à la première redirection
// Met un pointeur NULL à la place de la redirection et retourne la chaine après la redirection
char **splitOnRedir(char **command)
{
    do
    {
        if (strcmp(*(command), ">") == 0 ||
            strcmp(*(command), ">>") == 0 ||
            strcmp(*(command), "2>") == 0 ||
            strcmp(*(command), "2>>") == 0 ||
            strcmp(*(command), "<") == 0)
        {
            *(command) = NULL;
            return ++command;
        }
    } while (*(++command) != NULL);
    return command;
}

// Crée toute les redirections de gauche à droite
void createRedirections(char **command)
{
    char **nav = command;
    while (!(*nav == NULL || strcmp(*nav, "|") == 0))
    {
        if (strcmp(*(nav), ">") == 0)
        {
            nav++;
            createRedir(*nav, O_CREAT | O_TRUNC | O_WRONLY, S_IRWXU, 1);
        }
        else if (strcmp(*(nav), ">>") == 0)
        {
            nav++;
            createRedir(*nav, O_CREAT | O_APPEND | O_WRONLY, S_IRWXU, 1);
        }
        else if (strcmp(*(nav), "2>") == 0)
        {
            nav++;
            createRedir(*nav, O_CREAT | O_TRUNC | O_WRONLY, S_IRWXU, 2);
        }
        else if (strcmp(*(nav), "2>>") == 0)
        {
            nav++;
            createRedir(*nav, O_CREAT | O_APPEND | O_WRONLY, S_IRWXU, 2);
        }
        else if (strcmp(*(nav), "<") == 0)
        {
            nav++;
            createRedir(*nav, O_RDONLY, 0, 0);
        }
        nav++;
    }
}

// Créé une redirection du flux stdio vers le fichier défini dans path,
// avec les paramètres flag et createMode lorsque nécessaire
void createRedir(char *path, int flag, int createMode, int stdio)
{
    // vérifie le path de destination
    if (path != NULL && !checkSpecialChar(path))
    {
        // Redirection de stdio vers la sortie standard ou d'erreur
        if (strcmp(path, "&1") == 0 || strcmp(path, "&2") == 0){
            dup2((int)(path[1]-48), stdio);
            return;
        }
        int fd;
        //Crée le nouveau descripteur de fichier
        if ((fd = open(path, flag, createMode)) < 0)
        {
            perror(path);
            return;
        }
        //Redirige l' IO stdio passée en paramètre vers le fichier
        dup2(fd, stdio);
        close(fd);
    }
    else
    {
        sendParseError();
    }
}

// Cherche des caractères de redirection dans la chaine str
int checkSpecialChar(char *str)
{
    return !(strcmp(str, ">") ||
             strcmp(str, ">>") ||
             strcmp(str, "2>") ||
             strcmp(str, "2>>") ||
             strcmp(str, "<") ||
             strcmp(str, "|"));
}

// Affiche une erreur de parsing et quitte quand il n'y a pas de fichier après une redirection
void sendParseError()
{
    printf("Parse error for redirection\n");
    exit(1);
}

//Affiche le message str quand le mode debug est activé
void debug(char *str)
{
    if (DEBUG == 1)
    {
        printf("%s\n", str);
    }
}

int main(int argc, char **argv)
{
    char *prompt = "$";
    char *env;
    int finished = 0;
    char **input;
    do
    {
        //Essaye de récuperer le prompt depuis la variable d'environnement
        if ((env = getenv("INVITE")) != NULL)
        {
            prompt = env;
        }
        printf("%s ", prompt);
        fflush(stdout);
        input = lis_ligne();
        if (!fin_de_fichier(input))
        {
            if (!ligne_vide(input))
            {
                // Si l'entrée utilisateur n'est ni vide ni une fin de fichier , analyse la commande
                analyseCommand(input);
            }
        }
        else
        {
            finished = 1;
        }
    } while (finished != 1);
    return 0;
}